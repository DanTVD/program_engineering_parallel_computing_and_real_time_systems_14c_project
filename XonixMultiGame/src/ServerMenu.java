import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import static java.lang.Thread.sleep;

public class ServerMenu extends JPanel {

    private JLabel titleLbl;
    private JButton playBtn, highScoresBtn, quitBtn;

    private RunGamePanel rungp;
    private JFrame f;
    private ServerEnd s;

    public ServerMenu(JFrame f, ServerEnd s) {
        this.f = f;
        this.s = s;

        this.setLayout(new GridLayout(4, 1));

        titleLbl = new JLabel("Xonix Multiplayer by DZSH", JLabel.CENTER);

        playBtn = new JButton("Play");
        playBtn.addActionListener(e -> {
            //GameLobby gameLobby = new GameLobby(f);
            //gameLobby.start();

            //while (!ClientHandler.isGameReady());
            rungp = new RunGamePanel(f);
            rungp.start();

            s.updateRGPforClientHandlers(rungp);
        });

        highScoresBtn = new JButton("HighScores");
        highScoresBtn.addActionListener(e -> {
            JFrame tmp = new JFrame("HighScores");
            tmp.setSize(500, 400);

            ScoreTable ST = new ScoreTable(tmp, f);
            ST.setScoresatTable();
            ST.closeDB();
        });

        quitBtn = new JButton("Quit");
        quitBtn.addActionListener(e -> System.exit(0));

        this.add(titleLbl);
        this.add(playBtn);
        this.add(highScoresBtn);
        this.add(quitBtn);
    }

    public RunGamePanel getRungp() {
        return rungp;
    }
}
