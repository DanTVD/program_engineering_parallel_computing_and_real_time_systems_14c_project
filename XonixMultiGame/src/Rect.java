
import java.awt.*;
import java.io.Serializable;

public class Rect extends Thread implements Serializable {
	protected volatile int x, y, width, height;
	private Color color;
	private GamePanel panel;
	protected int dirx, diry;
	private Rectangle r;
	
	public Rect(int x,int y,int width, int height, Color color, GamePanel p){
		this.setX(x);
		this.setY(y);
		this.setWidth(width);
		this.setHeight(height);
		this.color=color;
		this.panel=p;
		this.r = new Rectangle(x, y, width, height);
	}

	public GamePanel getPanel() {
		return panel;
	}

	public void draw(Graphics g){
		g.setColor(color);
		//g.fillOval(x-width/2,y-width/2,width,width);
		g.fillRect(getX(),getY(),getWidth(),getHeight());
		//g.setColor(Color.BLACK);
		g.drawRect(getX(), getY(), getWidth(), getHeight());
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Rectangle getR() {
		return r;
	}

	public void run(){

		while (true) {



			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			panel.repaint();
		}

	}

	
	public double distance(int a,int b){
		return Math.sqrt(Math.pow(a, 2.0) + Math.pow(b,2.0));
	}
	
	public boolean overlaps (Rect r) {
	    return getX() < r.getX() + r.getWidth() && getX() + getWidth() > r.getX() && getY() < r.getY() + r.getHeight() && getY() + getHeight() > r.getY();
	}

	public boolean rectIntersectsRect(Rect other) {
		return other != null && distance(this.x - other.x, this.y - other.y) < this.width / 2 + other.width / 2 && overlaps(other);
	}

	public boolean BoxIntersectsBox ( Rect b)
	{
		Rectangle coverBox;
		Rectangle centerBox;

		int x1=x-b.width/2;
		int y1=y-b.width/2;
		int w1=width+b.width;
		int h1=height+b.width;

		coverBox=new Rectangle(x1,y1,w1,h1);
		centerBox=new Rectangle(b.x,b.y,1,1);

		if(centerBox.intersects(coverBox))
			return true;

		return coverBox.contains(b.x, b.y);

	}

	boolean intersects(Rect r) {
		Rectangle r1, r2;

		r1 = new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight());
		r2 = new Rectangle(r.getX(), r.getY(), r.getWidth(), r.getHeight());

		return r1.intersects(r2); // || r1.contains(r2.x, r2.y);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
}
