import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.Semaphore;

public class PlayerRect extends Rect {
    public enum PlayerTrail {P1, P2}
    public static Map<PlayerTrail, Color> P_Trails = new HashMap<>() {{
        put(PlayerTrail.P1, Color.LIGHT_GRAY); put(PlayerTrail.P2, Color.DARK_GRAY);
    }};
    private enum Direction {NONE, RIGHT, LEFT, DOWN, UP}

    private int id;
    private boolean isDead = false;

    private Direction dir;
    private static final int delta = 20;
    private static final int autoMoveDelay = 30;
    //private ArrayList<Rect> fill;
    //private ArrayList<Integer> xFill, yFill;
    //private static Semaphore trailFlood = new Semaphore(1);

    public PlayerRect(int x, int y, int width, int height, Color color, GamePanel p, int id) {
        super(x, y, width, height, color, p);
        dir = Direction.NONE;
        //fill = new ArrayList<>();
        this.id = id;
        //xFill = new ArrayList<>();
        //yFill = new ArrayList<>();
    }

    public void setRight() {

        if (getPanel().getLives(id) > 0 && dir != Direction.RIGHT && dir != Direction.LEFT && getX() + delta < getPanel().getWidth()) {
            setX(getX() + delta);
            this.getPanel().repaint();
            this.dir = Direction.RIGHT;
        }

    }

    public void setLeft() {
        if (getPanel().getLives(id) > 0 && dir != Direction.LEFT && dir != Direction.RIGHT && getX() - delta >= 0) {
            setX(getX() - delta);
            this.getPanel().repaint();
            this.dir = Direction.LEFT;
        }
    }

    public void setDown() {
        if (getPanel().getLives(id) > 0 && dir != Direction.DOWN && dir != Direction.UP && getY() + delta < getPanel().getHeight()) {
            setY(getY() + delta);
            this.getPanel().repaint();
            this.dir = Direction.DOWN;
        }
    }

    public void setUp() {
        if (getPanel().getLives(id) > 0 && dir != Direction.UP && dir != Direction.DOWN && getY() - delta >= 0) {
            setY(getY() - delta);
            this.getPanel().repaint();
            this.dir = Direction.UP;
        }
    }

    public void setNone() {

        if (dir != Direction.NONE) {
            setX(getX());
            setY(getY());
            this.getPanel().repaint();
            dir = Direction.NONE;
        }

    }

    @Override
    public void draw(Graphics g) {
        if (!isDead)
            super.draw(g);
        //g.setColor(Color.BLUE);
        //g.fillPolygon(xFill.stream().mapToInt(i->i).toArray(), yFill.stream().mapToInt(i->i).toArray(), xFill.size());
    }

    public void flood_fill(Color target, Color after, int maxX, int maxY, int minX, int minY, ArrayList<Rect> trail) {

        Rect other;
        Stack<Rect> Greys = new Stack<>();
        int countGreys = 0;
        boolean hasComp = false;

        /*System.out.println("Min X: " + minX + " | Max X: " + maxX);
        System.out.println("Min Y: " + minY + " | Max Y: " + maxY);
        System.out.println();*/

        if (minX < maxX && minY < maxY) {

            int CountLeft = 0, CountRight = 0, SumXLeft = 0, SumXRight = 0;
            boolean hasCompLeft = false, hasCompRight = false;
            Stack<Rect> Left = new Stack<>(), Right = new Stack<>();

            //if (maxX < (getPanel().getFrameRectsiLen() * 20) / 2 && maxY < (getPanel().getFrameRectsjLen() * 20) / 2)
            for (Rect i : trail) {
                for (int j = minX; j <= i.getX(); j++) {
                    //for (int k = minY; k <= i.getY(); k++) {
                    other = this.getPanel().getFrameRectAtij(j / 20, i.getY() / 20);
                    if (other.getColor() == target) {
                        CountLeft++;
                        SumXLeft += i.getX();
                        Left.add(other);
                    }

                    for (int k = minY; k <= i.getY(); k++)
                        hasCompLeft |= getPanel().hasCompAtXY(j, k);
                    //}
                }
            }

            //if (maxX > (getPanel().getFrameRectsiLen() * 20) / 2 && maxY > (getPanel().getFrameRectsjLen() * 20) / 2) {
                for (Rect i : trail) {
                    for (int j = i.getX(); j <= maxX; j++) {
                    //for (int k = minY; k <= i.getY(); k++) {
                        other = this.getPanel().getFrameRectAtij(j / 20, i.getY() / 20);
                        if (other.getColor() == target) {
                            CountRight++;
                            SumXRight += i.getX();
                            Right.add(other);
                        }

                        for (int k = i.getY(); k <= maxY; k++)
                            hasCompRight |= getPanel().hasCompAtXY(j, k);
                        //}
                    }
                }
           // }

            //System.out.println(hasCompLeft);
            //System.out.println(hasCompRight);
            if (dir != Direction.RIGHT && trail.get(0).getX() <  (getPanel().getFrameRectsiLen() * 20) / 2  || (CountRight == 0) || dir == Direction.LEFT ) { // {(CountLeft <= CountRight && CountLeft > 0
                System.out.println("Left");
                //System.out.println(dir);
                Greys = Left;
                countGreys = CountLeft;
                hasComp = hasCompLeft;
            }
            else if (trail.get(0).getX() > (getPanel().getFrameRectsiLen() * 20) / 2 || dir == Direction.RIGHT || dir == Direction.UP) { // CountLeft > CountRight {// minX > (getPanel().getFrameRectsiLen() * 20) / 2 && maxX > (getPanel().getFrameRectsiLen() * 20) / 2
                System.out.println("Right");
                //System.out.println(dir);
                Greys = Right;
                countGreys = CountRight;
                hasComp = hasCompRight;
            }

        }
        else if ((minX != maxX && minY == maxY) || (minY != maxY && minX == maxX)) {
            //System.out.println("Min X: " + minX + " | Max X: " + maxX);
            //System.out.println("Min Y: " + minY + " | Max Y: " + maxY);
            //System.out.println();

            int CountTill = 0, CountFrom = 0;
            boolean hasCompTill = false, hasCompFrom = false;
            Stack<Rect> Till = new Stack<>(), From = new Stack<>();

            if (minY == maxY) {
                for (Rect i : trail) {
                    for (int j = minX; j <= i.getX(); j++) {
                        for (int k = 0; k <= i.getY(); k++) {
                            other = this.getPanel().getFrameRectAtij(j / 20, k / 20);
                            if (other.getColor() == target && !getPanel().hasCompAtXY(j, k)) {
                                CountTill++;
                                Till.add(other);
                            }

                            hasCompTill |= getPanel().hasCompAtXY(j, k);
                        }
                    }
                }

                for (Rect i : trail) {
                    for (int j = minX; j <= i.getX(); j++) {
                        for (int k = i.getY(); k < getPanel().getFrameRectsjLen() * 20; k++) {
                            other = this.getPanel().getFrameRectAtij(j / 20, k / 20);
                            if (other.getColor() == target && !getPanel().hasCompAtXY(j, k)) {
                                CountFrom++;
                                From.add(other);
                            }

                            hasCompFrom |= getPanel().hasCompAtXY(j, k);
                        }
                    }
                }

            }
            else if (minX == maxX) {
                for (Rect i : trail) {
                    for (int j = 0; j <= i.getX(); j++) {
                        for (int k = minY; k <= i.getY(); k++) {
                            other = this.getPanel().getFrameRectAtij(j / 20, k / 20);
                            if (other.getColor() == target && !getPanel().hasCompAtXY(j, k)) {
                                CountTill++;
                                Till.add(other);
                            }

                            hasCompTill |= getPanel().hasCompAtXY(j, k);
                        }
                    }
                }

                for (Rect i : trail) {
                    for (int j = i.getX(); j < getPanel().getFrameRectsiLen() * 20; j++) {
                        for (int k = minY; k <= i.getY(); k++) {
                            other = this.getPanel().getFrameRectAtij(j / 20, k / 20);
                            if (other.getColor() == target && !getPanel().hasCompAtXY(j, k)) {
                                CountFrom++;
                                From.add(other);
                            }

                            hasCompFrom |= getPanel().hasCompAtXY(j, k);
                        }
                    }
                }

            }

            if (CountTill <= CountFrom) { //&& CountTill > 0
                Greys = Till;
                countGreys = CountTill;
                hasComp = hasCompTill;
            }
            else {  // if CountTill > CountFrom
                Greys = From;
                countGreys = CountFrom;
                hasComp = hasCompFrom;
            }

            //System.out.println(countGreys);
        } else if (minY == maxY && minX == maxX) {
            System.out.println("Min X: " + minX + " | Max X: " + maxX);
            System.out.println("Min Y: " + minY + " | Max Y: " + maxY);
            System.out.println();

            int CountTill = 0, CountFrom = 0;
            boolean hasCompTill = false, hasCompFrom = false;
            Stack<Rect> Till = new Stack<>(), From = new Stack<>();

            for (Rect i : trail) {
                for (int j = 0; j <= i.getX(); j++) {
                    for (int k = 0; k <= i.getY(); k++) {
                        other = this.getPanel().getFrameRectAtij(j / 20, k / 20);
                        if (other.getColor() == target && !getPanel().hasCompAtXY(j, k)) {
                            CountTill++;
                            Till.add(other);
                        }

                        hasCompTill |= getPanel().hasCompAtXY(j, k);
                    }
                }
            }

            for (Rect i : trail) {
                for (int j = i.getX(); j < getPanel().getFrameRectsiLen() * 20; j++) {
                    for (int k = i.getY(); k < getPanel().getFrameRectsjLen() * 20; k++) {
                        other = this.getPanel().getFrameRectAtij(j / 20, k / 20);
                        if (other.getColor() == target && !getPanel().hasCompAtXY(j, k)) {
                            CountFrom++;
                            From.add(other);
                        }

                        hasCompFrom |= getPanel().hasCompAtXY(j, k);
                    }
                }
            }

            if (CountTill <= CountFrom) { //&& CountTill > 0
                Greys = Till;
                countGreys = CountTill;
                hasComp = hasCompTill;
            }
            else {  // if CountTill > CountFrom
                Greys = From;
                countGreys = CountFrom;
                hasComp = hasCompFrom;
            }

        }

        if (!hasComp) {

            for (Rect i : Greys) {
                i.setColor(after);
            }

            getPanel().incScore(countGreys, id);
        }

    }

    public void run() {
        boolean isHit, isSpikehit, isAfter = false;
        ArrayList<Rect> frameOfFill = new ArrayList<>();

        while (true) {
            isHit = false;
            isSpikehit = false;
            boolean isPaused = getPanel().isPaused();
            //if (!xFill.isEmpty()) xFill.clear();
            //if (!yFill.isEmpty()) yFill.clear();

            Rect other;

            // Life Handling: Comps Collision
            for (int i = 0; i < this.getPanel().getCompRectsLen() && !isHit; i++) {
                other = this.getPanel().getCompRectAti(i);

                if (other != null && this.intersects(other) && other.getColor() != Color.GRAY &&
                        (id == 0 && other.getColor() != PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P1)) ||
                        (id == 1 && other.getColor() != PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P2)))) &&
                        this.getPanel().getFrameRectAtij(x / 20, y / 20).getColor() != Color.BLUE)
                {
                    isHit = true;
                    break;
                }
            }

            // Life Handling: Spikes Collision
            for (int i = 0; i < this.getPanel().getSpikeCompRectsLen() && !isHit; i++) {
                other = this.getPanel().getSpikeCompRectAti(i);

                if (other != null && this.intersects(other) && other.getColor() != Color.GRAY && (id == 0 && other.getColor() != PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P1)) || (id == 1 && other.getColor() != PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P2))))) {
                    isSpikehit = true;
                    break;
                }
            }

            // Trail Handling: Creation of Trail
            for (int i = 0; i < this.getPanel().getFrameRectsiLen(); i++) {
                for (int j = 0; j < this.getPanel().getFrameRectsjLen(); j++) {
                    other = this.getPanel().getFrameRectAtij(i, j);

                    if (this.x == other.x && this.y == other.y && other.getColor() == Color.GRAY) {
                        frameOfFill.add(other);
                        if (id == 0) other.setColor((PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P1))));
                        else if (id == 1) other.setColor((PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P2))));
                        //flood_fill(x, y, Color.GRAY, Color.BLUE, false);
                    }
                }
            }

            // Life Handling: Comps Collision at Trail
            for (int i = 0; i < this.getPanel().getCompRectsLen() && !isHit; i++) {
                other = this.getPanel().getCompRectAti(i);

                for (Rect j : frameOfFill) {
                    if (other != null && j.intersects(other) && other.getColor() != Color.GRAY && (id == 0 && other.getColor() != PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P1)) || (id == 1 && other.getColor() != PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P2))))) {
                        isHit = true;
                        break;
                    }
                }
            }

            //flood_fill(x, y, Color.LIGHT_GRAY, Color.BLUE, false);
            //frameOfFill.addAll(fill);
            //Fill prevMode = null;

            //trailFlood.acquire();


            if (this.x / 20 < this.getPanel().getFrameRectsiLen() && this.y / 20 < this.getPanel().getFrameRectsjLen() && this.getPanel().getFrameRectAtij(this.x / 20, this.y / 20).getColor() == Color.BLUE) {
                //flood_fill(x, y, Color.LIGHT_GRAY, Color.BLUE);
                int maxX = 0, maxY = 0;
                int minX = getPanel().getFrameRectsiLen() * 20, minY = getPanel().getFrameRectsjLen() * 20;
                for (Rect i : frameOfFill) {
                    i.setColor(Color.BLUE);
                    //System.out.println(i.getX() + " " + i.getY());
                    maxX = Integer.max(i.getX(), maxX);
                    maxY = Integer.max(i.getY(), maxY);

                    minX = Integer.min(i.getX(), minX);
                    minY = Integer.min(i.getY(), minY);

                    // xFill.add(i.x);
                    // yFill.add(i.y);
                }
                //maxX = maxX == 0 ? x : maxX;
                //maxY = maxY == 0 ? y : maxY;
                //minX = minY == getPanel().getFrameRectsiLen() - 1 ? x : minX;
                //minY = minY == getPanel().getFrameRectsjLen() - 1 ? y : minY;

                this.getPanel().incScore(frameOfFill.size(), id);
                if (maxX != 0 && maxY != 0 && minY != getPanel().getFrameRectsiLen() * 20 && minY != getPanel().getFrameRectsjLen() * 20) {
                    flood_fill(Color.GRAY, Color.BLUE, maxX, maxY, minX, minY, frameOfFill);
                    SoundEffects.playFill();
                }

                frameOfFill.clear();
                //System.out.println(maxX + " " + maxY);
                //flood_fill(Color.GRAY, Color.BLUE, x, y);
                //prevMode = flood_fill(Color.GRAY, Color.BLUE, maxX, maxY, minX, minY, Fill.LT, prevMode); // left till
                //prevMode = flood_fill(Color.GRAY, Color.BLUE, maxX, maxY, minX, minY, Fill.RF, prevMode); // right from
                //prevMode = flood_fill(Color.GRAY, Color.BLUE, maxX, maxY, minX, minY, Fill.RT, prevMode); // right till
                //prevMode = flood_fill(Color.GRAY, Color.BLUE, maxX, maxY, minX, minY, Fill.LF, prevMode); // left from


                //fill.clear();
            }
            //trailFlood.release();
            /*else {
                for (int i = 0; i < frameOfFill.size(); i++) {
                    frameOfFill.get(i).setColor(Color.GRAY);
                frameOfFill.clear();
            }*/

                //System.out.printf("{%d, %d}\n", x / GamePanel.width, y / GamePanel.height);

            if (0 <= this.x && this.x / 20 < this.getPanel().getFrameRectsiLen() && 0 <= this.y && this.y / 20 < this.getPanel().getFrameRectsjLen() && (id == 0 && this.getPanel().getFrameRectAtij(this.x / 20, this.y / 20).getColor() == PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P1)) || (id == 1 && this.getPanel().getFrameRectAtij(this.x / 20, this.y / 20).getColor() == PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P2))))) {
                if (dir == Direction.RIGHT) {
                    if ((x / 20) - 1 >= 0) {
                        other = getPanel().getFrameRectAtij((x / 20) - 1, y / 20);
                        if (other.getColor() != Color.BLUE) {
                            frameOfFill.add(other);
                            if (id == 0) other.setColor(PlayerRect.P_Trails.get(PlayerRect.PlayerTrail.P1));
                            else if (id == 1) other.setColor(PlayerRect.P_Trails.get(PlayerRect.PlayerTrail.P2));
                        }
                    }
                    setX(getX() + delta); // if (getPanel().getFrameRectAtij((getX() + delta) / 20, getY() / 20).getColor() != Color.LIGHT_GRAY)

                    try {
                        sleep(autoMoveDelay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else if (dir == Direction.LEFT) {
                    if ((x / 20) + 1 < this.getPanel().getFrameRectsiLen()) {
                        other = getPanel().getFrameRectAtij((x / 20) + 1, y / 20);
                        if (other.getColor() != Color.BLUE) {
                            frameOfFill.add(other);
                            if (id == 0) other.setColor(PlayerRect.P_Trails.get(PlayerRect.PlayerTrail.P1));
                            else if (id == 1) other.setColor(PlayerRect.P_Trails.get(PlayerRect.PlayerTrail.P2));
                        }
                    }
                    setX(getX() - delta); //if (getPanel().getFrameRectAtij((getX() - delta) / 20, getY() / 20).getColor() != Color.LIGHT_GRAY)

                    try {
                        sleep(autoMoveDelay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else if (dir == Direction.DOWN) {
                    if ((y / 20) - 1 >= 0) {
                        other = getPanel().getFrameRectAtij(x / 20, (y / 20) - 1);
                        if (other.getColor() != Color.BLUE) {
                            frameOfFill.add(other);
                            if (id == 0) other.setColor(PlayerRect.P_Trails.get(PlayerRect.PlayerTrail.P1));
                            else if (id == 1) other.setColor(PlayerRect.P_Trails.get(PlayerRect.PlayerTrail.P2));
                        }
                    }
                    setY(getY() + delta); // if (getPanel().getFrameRectAtij(getX() / 20, (getY() + delta) / 20).getColor() != Color.LIGHT_GRAY)

                    try {
                        sleep(autoMoveDelay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else if (dir == Direction.UP) {
                    if ((y / 20) + 1 < this.getPanel().getFrameRectsjLen()) {
                        other = getPanel().getFrameRectAtij(x / 20, (y / 20) + 1);
                        if (other.getColor() != Color.BLUE) {
                            frameOfFill.add(other);
                            if (id == 0) other.setColor(PlayerRect.P_Trails.get(PlayerRect.PlayerTrail.P1));
                            else if (id == 1 ) other.setColor(PlayerRect.P_Trails.get(PlayerRect.PlayerTrail.P2));
                        }
                    }
                    setY(getY() - delta); // if (getPanel().getFrameRectAtij(getX() / 20, (getY() - delta) / 20).getColor() != Color.LIGHT_GRAY)

                    try {
                        sleep(autoMoveDelay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                dir = Direction.NONE;
            }

            if (isHit && this.getPanel().getLives(id) > 0) {

                for (int i = 0; i < this.getPanel().getFrameRectsiLen() && !isAfter; i++) {
                    for (int j = 0; j < this.getPanel().getFrameRectsjLen() && !isAfter; j++) {
                        other = this.getPanel().getFrameRectAtij(i, j);

                        if ((other.getColor() == Color.GRAY || ((other.getColor() == PlayerRect.P_Trails.get(PlayerRect.PlayerTrail.P1)) || (other.getColor() == PlayerRect.P_Trails.get(PlayerRect.PlayerTrail.P2))) && other.getX() == this.x && other.getY() == this.y)) {

                            for (Rect k : frameOfFill) {
                                k.setColor(Color.GRAY);
                            }
                            frameOfFill.clear();

                            this.getPanel().lostLive(id);
                            if (getPanel().getLives(id) == 0) isDead = true;
                            isAfter = true;
                        }
                    }
                 }

                SoundEffects.playLoseLife();
                //this.getPanel().lostLive();
                if (isAfter) {
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    break;
                }

            }
            else if (isSpikehit && this.getPanel().getLives(id) > 0) {
                for (int i = 0; i < this.getPanel().getFrameRectsiLen() && !isAfter; i++) {
                    for (int j = 0; j < this.getPanel().getFrameRectsjLen() && !isAfter; j++) {
                        other = this.getPanel().getFrameRectAtij(i, j);

                        if (other.getColor() == Color.BLUE && other.getX() == this.x && other.getY() == this.y) {
                            for (Rect k : frameOfFill) {
                                k.setColor(Color.GRAY);
                            }
                            frameOfFill.clear();

                            this.getPanel().lostLive(id);
                            if (getPanel().getLives(id) == 0) isDead = true;
                            isAfter = true;
                        }
                    }
                }

                SoundEffects.playLoseLife();
                //this.getPanel().lostLive();
                if (isAfter) {
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    break;
                }
            }

            if (isPaused) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                Thread.sleep(5); // 100
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.getPanel().repaint();
        }
    }
}

