import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class Date {
    private static String pattern = "dd.MM.yyyy_HH-mm-ss";

    static String getDate() {

        // Create an instance of SimpleDateFormat used for formatting
        // the string representation of date according to the chosen pattern
        DateFormat df = new SimpleDateFormat(pattern);

        // Get the today date using Calendar object.
        java.util.Date today = Calendar.getInstance().getTime();
        // Using DateFormat format method we can create a string
        // representation of a date with the defined format.
        String todayAsString = df.format(today);

        // Print the result!
        return todayAsString;
    }

    static java.util.Date stringToDate(String date) {
        DateFormat df = new SimpleDateFormat(pattern);

        try {
            return df.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static long getDateDiff(java.util.Date date1, java.util.Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

    static boolean isDateToDelete(String date) {
        java.util.Date newDate = stringToDate(date);

        java.util.Date today = Calendar.getInstance().getTime();

        //System.out.println(getDateDiff(newDate, today, TimeUnit.MINUTES));
        return getDateDiff(newDate, today, TimeUnit.MINUTES) >= 1;
    }

    static void clearUnusedDates(File[] files) {

        for (int i = 0; i < files.length; i++) {
            //System.out.println(files[i].getName().substring(0, files[i].getName().length() - 3 - 1));

            if (isDateToDelete(files[i].getName().substring(0, files[i].getName().length() - 3 - 1))) {

                if(files[i].delete())
                {
                    System.out.println("File deleted successfully");
                }
                else
                {
                    System.out.println("Failed to delete the file");
                }
            }
        }

        //System.out.println();
    }
}
