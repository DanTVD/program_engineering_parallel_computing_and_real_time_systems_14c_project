

import javax.swing.JFrame;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import static java.lang.Thread.sleep;

public class ClientEnd {
	
	private Socket socket;
	//private RunGameFrame rgf;
	//private Executor ex;
	private JFrame f;
	//private GameFrame gf;
	private GameController gc;
	private ClientRecv cv;
	public volatile ArrayDeque<String> q;
	private Semaphore q_mtx;

	private int currID;

	//private RectData[][] frameR;
	//private RectData[] compR, playerR, compSpikeR;

    public ClientEnd()
    {
		//ex = Executors.newFixedThreadPool(1);
		this.q_mtx = new Semaphore(1);
		this.q = new ArrayDeque<>();

		do {
			try {
				socket = new Socket("127.0.0.1", ServerEnd.port);// MS 127.0.0.1 is IP of local host
				break;
			} catch (IOException e) {
				//e.printStackTrace();
				System.out.println("Trying to find Server...");
				try {
					sleep(1000);
				} catch (InterruptedException ex) {
					throw new RuntimeException(ex);
				}
			}
		} while (true);

		this.cv = new ClientRecv(socket, q_mtx, this);
		cv.start();

		while (q.isEmpty());

		try {
			q_mtx.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.currID = Integer.parseInt(q.remove());
		System.out.println("msg removed from q");
		q_mtx.release();

		System.out.println(currID);

		f = new JFrame("Xonix Client Multiplayer by DZSH");
		gc = new GameController(socket, currID);

		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(500, 400);
		//this.f.add(gf);
		this.f.add(gc);
		f.setResizable(false);
		f.setVisible(true);
		f.setFocusable(false);

	}

	private void recvFromServer() {
		try {

			DataInputStream input = new DataInputStream(socket.getInputStream());
			String string = input.readUTF();
			System.out.println("Server: " + string);
		} catch (Exception ev) {
			System.out.println("Network Problem ");

			try {
				socket.close();
				sleep(5000);
				System.exit(0);
			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	/*private GameFrame recvFromServerObj() {
		try {
			ObjectInputStream input = new ObjectInputStream(socket.getInputStream());

			frameR = (RectData[][]) input.readObject();
			compR = (RectData[]) input.readObject();
			playerR = (RectData[]) input.readObject();
			compSpikeR = (RectData[]) input.readObject();
			//Stats stats = (Stats) input.readObject();
			//System.out.println("Server: " + string);
			return new GameFrame(frameR, compR, playerR, compSpikeR); //(GameFrame) input.readObject();
		} catch (Exception ev) {
			ev.printStackTrace();
			System.out.println("Network Problem ");

			try {
				socket.close();
				sleep(5000);
				System.exit(0);
			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}*/

	private void sendToServer() {
		//System.out.println("Client : ");
		try {
			DataOutputStream output = new DataOutputStream(socket.getOutputStream());
			output.writeUTF(new Scanner(System.in).next());
		} catch (IOException e1) {
			System.out.println("Network issues");
			try {
				sleep(2000);
				System.exit(0);
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}

		}
	}
    

    public void clientConnection()
	{
		//socket = new Socket("localhost", 8080); // MS  als0 works
		String msg;
		while (true) {
			/*ex.execute(() -> {
				gf = recvFromServerObj();
					f.add(gf);
					f.setVisible(true);
					try {
						sleep(5000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					f.remove(gf);
					gf = null;
					System.gc();
					//recvFromServer();
			});*/

			//sendToServer();
			while (q.isEmpty());

			try {
				q_mtx.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			msg = q.remove();
			System.out.println("msg removed from q");
			q_mtx.release();

			System.out.println(msg);
			String[] splitMsg = msg.split(" ");

			String res = "";
			if (splitMsg[0].equals("INFO")) {
				for (int i = 1; i < splitMsg.length; i++) {
					res += splitMsg[i] + " ";
				}

				gc.setInfo(res);
			}
			else if (splitMsg[0].equals("NEWGAME")) {
				gc.resetUserControls();
			}
		}
	}
  
	public static void main(String[] args) throws IOException
	{
		ClientEnd c = new ClientEnd();
		c.clientConnection();  
	}
}
