import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class SoundEffects extends Thread implements Serializable {

    private static final File loseLife = new File("sounds/sfx_sounds_negative1.wav");
    private static final File fill = new File("sounds/sfx_sounds_powerup2.wav");
    private static final File win = new File("sounds/mixkit-medieval-show-fanfare-announcement-226.wav");
    private static final File lose = new File("sounds/mixkit-ominous-drums-227.wav");
    private static final File BG = new File("sounds/Komiku_-_51_-_Chocolate_Valley.wav");

    private boolean isRunning = true;
    private Clip clip;

    GamePanel panel;

    public SoundEffects(GamePanel panel) {
        super();
        this.panel = panel;
    }

    static void playSound(File file) {
        try {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(file));
            clip.start();
        } catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }
    }

    static double getSoundLen(File file) throws UnsupportedAudioFileException, IOException {
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
        AudioFormat format = audioInputStream.getFormat();
        long frames = audioInputStream.getFrameLength();
        double durationInSeconds = (frames+0.0) / format.getFrameRate();

        return durationInSeconds;
    }

    void playSoundInf(File file) throws InterruptedException {
        try {
            clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(file));
            clip.start();
            Thread.sleep((long) getSoundLen(file) * 1000);
        } catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }

    }

    static void playLoseLife() {
        playSound(loseLife);
    }

    static void playFill() {
        playSound(fill);
    }

    static void playWin() {
        playSound(win);
    }

    static void playLose() {
        playSound(lose);
    }

    @Override
    public void run() {
        while (isRunning) {
            try {
                playSoundInf(BG);
                Thread.sleep(5);
            } catch (InterruptedException e) {
                this.panel.setPaused(true);
                isRunning = false;
                clip.stop();
                break;
            }

        }
        System.out.println("Ended BGM");

    }
}
