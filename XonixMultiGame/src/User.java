import javax.swing.*;

public class User {
    private int id;
    private GamePanel panel;

    private int score;
    private int lifes;
    private String username;
    private volatile JLabel label;

    public User(int id, GamePanel panel) {
        this.id = id;
        this.panel = panel;

        this.score = 0;
        this.lifes = 3;
        this.username = "";
        this.label = new JLabel("P" + (this.id + 1) + ": " + this.score + "pts, " + this.lifes + "♥ |", SwingConstants.CENTER);
        //this.label.setForeground(panel.getPlayerRectAti(id).getColor());
    }

    public User(int id) {
        this(id, null);
    }

    public void setPanel(GamePanel panel) {
        this.panel = panel;
    }

    public void setup() {
        this.label.setForeground(panel.getPlayerRectAti(id).getColor());
    }

    public int getScore() {
        return score;
    }

    public int getLifes() {
        return lifes;
    }

    public void removeLife() {
        this.lifes--;
    }

    public void resetLifes() {
        this.lifes = 3;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public String getUsername() {
        return username.isEmpty() ? "P" + (this.id + 1) : username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public JLabel getLabel() {
        if (username.isEmpty())
            this.label.setText("P" + (this.id + 1) + ": " + score + "pts, " + lifes + "♥ |");
        else {
            this.label.setText(username + ": " + score + "pts, " + lifes + "♥ |");
        }
        return label;
    }
}
