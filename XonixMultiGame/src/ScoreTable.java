import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Objects;

public class ScoreTable extends JPanel {

    private SQLite sqLite;
    private JFrame f, MenuF;

    private static final String createTable = "CREATE TABLE IF NOT EXISTS HighScores (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "P1_username TEXT NOT NULL," +
            "P1_score INTEGER NOT NULL," +
            "P2_username TEXT NOT NULL," +
            "P2_score INTEGER NOT NULL," +
            "status TEXT NOT NULL" +
            ")";

    private static final String selectTable = "SELECT * FROM HighScores";

    public ScoreTable(JFrame f, JFrame MenuF) {
        this.sqLite = new SQLite();
        this.sqLite.query(createTable);

        this.f = f;
        this.MenuF = MenuF;

        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                MenuF.setVisible(true);
            }

            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                MenuF.setVisible(false);
            }
        });

    }

    public void insertScore(User P1, User P2) {
        String status = "";


        if (P1.getScore() > P2.getScore()) status = P1.getUsername() + " Won!";
        else if (P1.getScore() < P2.getScore()) status = P2.getUsername() + " Won!";
        else if (P1.getScore() == P2.getScore()) status = "Tie!";

        String insertQuery = MessageFormat.format("INSERT INTO HighScores (P1_username, P1_score, P2_username, P2_score, status)" +
                " VALUES (''{0}'', {1}, ''{2}'', {3}, ''{4}'')", P1.getUsername(), Integer.toString(P1.getScore()), P2.getUsername(),
                Integer.toString(P2.getScore()), status);

        System.out.println(insertQuery);

        this.sqLite.query(insertQuery);
    }

    public void setScoresatTable() {

        String[] columnNames = {//"id",
                "P1 Username",
                "P1 Score",
                "P2 Username",
                "P2 Score",
                "Status"};

        ResultSet rs = this.sqLite.queryWithResult(selectTable);
        Object[][] selectQuery;
        int rowCount = 0;

        try {
            while (rs.next()) {
                rs.getInt("id");
                rs.getString("P1_username");
                rs.getInt("P1_score");
                rs.getString("P2_username");
                rs.getInt("P2_score");
                rs.getString("status");
                rowCount++;
            }
        } catch (Exception e) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }

        this.sqLite.closeQueryWithResult();

        rs = this.sqLite.queryWithResult(selectTable);
        selectQuery = new Object[rowCount][columnNames.length];

        try {
            for (int i = 0; i < selectQuery.length && rs.next(); i++) {
                rs.getInt("id");
                Object[] objects = {
                        rs.getString("P1_username"),
                        rs.getInt("P1_score"),
                        rs.getString("P2_username"),
                        rs.getInt("P2_score"),
                        rs.getString("status")};

                selectQuery[i] = objects;
            }
        } catch (Exception e) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }

        this.sqLite.closeQueryWithResult();


        JTable j = new JTable(selectQuery, columnNames);
        j.setBounds(30, 40, 200, 300);

        JScrollPane sp = new JScrollPane(j);

        f.add(sp);
        //f.setSize(500, 200);
        f.setVisible(true);
    }

    public void openDB() {
        this.sqLite.openDB();
    }
    public void closeDB() {
        this.sqLite.closeDB();
    }
}
