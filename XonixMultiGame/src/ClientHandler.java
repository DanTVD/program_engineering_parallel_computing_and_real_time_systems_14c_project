import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Objects;
import java.util.Scanner;
import java.awt.Robot.*;

public class ClientHandler extends Thread {

    private Socket socket;
    //private Executor ex;
    private RunGamePanel rgp;
    private static int clientID = 0;
    private int currID;

    public static final int MAX_PLAYER_COUNT = 2;

    private ServerSend ss;

    ClientHandler(Socket clientSock, RunGamePanel rgp) {
        this.socket = clientSock;
        //ex = Executors.newFixedThreadPool(1);
        this.rgp = rgp;

        this.currID = clientID;
        clientID++;

        ss = new ServerSend(socket, currID, rgp);
    }

    /*private RectData[] RectArrToRectDataArr(Rect[] arr) {
        int loc = 0;
        RectData[] newArr = new RectData[arr.length];

        for (Rect rect : arr) {
            newArr[loc] = new RectData(rect.x, rect.y, rect.width, rect.height, rect.getColor());
            loc++;
        }

        return newArr;
    }

    private RectData[][] Rect2DArrToRect2DDataArr(Rect[][] arr) {
        int loc = 0;
        RectData[][] newArr = new RectData[arr.length][arr[0].length];

        for (Rect[] rectArr : arr) {
            newArr[loc] = RectArrToRectDataArr(rectArr);
            loc++;
        }
        return newArr;
    }*/

    private String recvFromClient() {
        try {

            DataInputStream input = new DataInputStream(socket.getInputStream());
            String string = input.readUTF();
            //System.out.println("Client: " + string);
            return string;
        } catch (Exception ev) {
            System.out.println("Network Problem");

            try {
                socket.close();
                Thread.sleep(5000);
                System.exit(0);
            } catch (InterruptedException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }

    private void sendToClient() {
        //System.out.println("Server : ");
        try {
            DataOutputStream output = new DataOutputStream(socket.getOutputStream());
            output.writeUTF(new Scanner(System.in).next());
        } catch (IOException e1) {
            System.out.println("Network Problem");
            try {
                Thread.sleep(2000);
                System.exit(0);
            } catch (InterruptedException e2) {

                e2.printStackTrace();
            }
        }
    }

    /*private void sendToClientObj() {
        //System.out.println("Server : ");
        try {
            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());

            output.writeObject(Rect2DArrToRect2DDataArr(rgp.getGp().getFrameRects()));
            output.writeObject(RectArrToRectDataArr(rgp.getGp().getCompRects()));
            output.writeObject(RectArrToRectDataArr(rgp.getGp().getPlayerRects()));
            output.writeObject(RectArrToRectDataArr(rgp.getGp().getCompSpikeRects()));

            /*output.writeObject(new GameFrame(Rect2DArrToRect2DDataArr(rgp.getGp().getFrameRects()),
                    RectArrToRectDataArr(rgp.getGp().getCompRects()),
                    RectArrToRectDataArr(rgp.getGp().getPlayerRects()),
                    RectArrToRectDataArr(rgp.getGp().getCompSpikeRects()))
            );*/
            /*
            System.gc();
            //output.writeObject();
            //output.writeObject(gp.getStats());
            //output.writeObject(gf);
            //System.out.println(rgp.getGp());
        } catch (IOException e1) {
            e1.printStackTrace();
            System.out.println("Network Problem");
            try {
                Thread.sleep(2000);
                System.exit(0);
            } catch (InterruptedException e2) {

                e2.printStackTrace();
            }
        }
    }*/

    public static boolean isGameReady() {
        return ClientHandler.MAX_PLAYER_COUNT == ClientHandler.clientID;
    }

    public void setRgp(RunGamePanel rgp) {
        this.rgp = rgp;
        this.ss.setRunning(false);
        this.ss = new ServerSend(socket, currID, rgp);
        this.ss.start();
    }

    @Override
    public void run() {
        String msg;
        /*Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        assert robot != null;*/

        ss.start();

        while (true) {
            //System.out.println(rgp.getGp().getUserAti(currID).getLabel().getText());

            /*ex.execute(() -> {
                while (true) {
                    sendToClientObj();
                    try {
                        sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                    //sendToClientObj(); //sendToClient();

            });*/

            msg = recvFromClient();
            String[] splitMsg = msg.split(" ");

            if(Integer.parseInt(splitMsg[0]) == 0) {
                System.out.print("P1: ");

                System.out.println(splitMsg[1]);

                /*rgp.getGp().requestFocus();
                if (splitMsg[1].equals("RIGHT")) {
                    robot.keyPress(KeyEvent.VK_RIGHT);
                } else if (splitMsg[1].equals("LEFT")) {
                    robot.keyPress(KeyEvent.VK_LEFT);
                } else if (splitMsg[1].equals("UP")) {
                    robot.keyPress(KeyEvent.VK_UP);
                } else if (splitMsg[1].equals("DOWN")) {
                    robot.keyPress(KeyEvent.VK_DOWN);
                }*/

            } else if (Integer.parseInt(splitMsg[0]) == 1) {
                System.out.print("P2: ");

                System.out.println(splitMsg[1]);

                /*rgp.getGp().requestFocus();
                if (splitMsg[1].equals("RIGHT")) {
                    robot.keyPress(KeyEvent.VK_D);
                } else if (splitMsg[1].equals("LEFT")) {
                    robot.keyPress(KeyEvent.VK_A);
                } else if (splitMsg[1].equals("UP")) {
                    robot.keyPress(KeyEvent.VK_W);
                } else if (splitMsg[1].equals("DOWN")) {
                    robot.keyPress(KeyEvent.VK_S);
                }*/
            }
            else {
                continue;
            }

            String res = "";

            if (splitMsg[1].equals("RIGHT") || splitMsg[1].equals("LEFT") || splitMsg[1].equals("UP") || splitMsg[1].equals("DOWN")) {
                rgp.getGp().controlPlayerati(Integer.parseInt(splitMsg[0]), splitMsg[1]);
            } else if (splitMsg[1].equals("USERNAME")) {
                for (int i = 2; i < splitMsg.length; i++) {
                    res += splitMsg[i] + " ";
                }

                rgp.getGp().getUserAti(Integer.parseInt(splitMsg[0])).setUsername(res);
            } else if (splitMsg[1].equals("RESUME")) {
                rgp.getGp().resumeGame();
            } else if (splitMsg[1].equals("PAUSE")) {
                rgp.getGp().pauseGame();
            } else if (splitMsg[1].equals("SCREENSHOT")) {
                rgp.getGp().takeScreenshotNow();
            }

        }
    }
}
