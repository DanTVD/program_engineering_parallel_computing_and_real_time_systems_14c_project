import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class GameFrame extends JPanel implements Serializable {
    //public static final int height = 800 - 14;
    //public static final int width = 800 - 37;
    private RectFrame[] compRects;
    private RectFrame[] compSpikeRects;
    private RectFrame[][] frameRects;
    private RectFrame[] playerRects;
    private Stats stats;

    public void createFrame(RectData[][] originalFrameRects, int row, int col) {
        frameRects = new RectFrame[row][col];
        //System.out.println("H: " + frameRects.length + " | W: " + frameRects[0].length);
        int rWidth = 20;

        for (int i = 0; i < frameRects.length; i++) {
            for (int j = 0; j < frameRects[i].length; j++) {
                frameRects[i][j] = new RectFrame(originalFrameRects[i][j].getX(), originalFrameRects[i][j].getY(), rWidth, rWidth, originalFrameRects[i][j].getColor(), this);
                frameRects[i][j].start();
            }
        }

        //System.out.println(frameRects[frameRects.length - 1][frameRects[0].length - 1].getX() + " " + frameRects[frameRects.length - 1][frameRects[0].length - 1].getY());

    }

    public void createComps(RectData[] originalComps, int len) {
        compRects = new RectFrame[len];
        int rWidth = 15;

        for (int i = 0; i < compRects.length; i++) {
            compRects[i] = new RectFrame(originalComps[i].getX(), originalComps[i].getY(), rWidth, rWidth, Color.RED, this);
            compRects[i].start();
        }
    }

    public void createSpikeComps(RectData[] originalSpikes, int len) {
        compSpikeRects = new RectFrame[len];
        int rWidth = 15;

        for (int i = 0; i < compSpikeRects.length; i++) {
            compSpikeRects[i] = new RectFrame(originalSpikes[i].getX(), originalSpikes[i].getY(), rWidth, rWidth, Color.GREEN, this);
            compSpikeRects[i].start();
        }
    }

    public void createPlayer(int i, RectData originalPlayer) {

        int rWidth = 20;
        Color c = null;
        if (i == 0)
            c = Color.ORANGE;
        else if (i == 1)
            c = Color.YELLOW;
        playerRects[i] = new RectFrame(originalPlayer.getX(), originalPlayer.getY(), rWidth, rWidth, c, this);
        playerRects[i].start();
    }

    public void createPlayers(RectData[] originalPlayers, int len) {
        playerRects = new RectFrame[len];
        for (int i = 0; i < playerRects.length; i++)
            createPlayer(i, originalPlayers[i]);
    }

    public GameFrame(RectData[][] originalFrame, RectData[] originalComps, RectData[] originalPlayers, RectData[] originalSpikes) { //, Stats originalStats
        createFrame(originalFrame, originalFrame.length, originalFrame[0].length);
        createComps(originalComps, originalComps.length);
        createPlayers(originalPlayers, originalPlayers.length);
        createSpikeComps(originalSpikes, originalSpikes.length);
        //stats = originalStats;

        setBackground(Color.GRAY);
        setFocusable(true);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);


        for (RectFrame[] frameRect : frameRects) {
            for (RectFrame frameRectij : frameRect)
                if (frameRectij != null) {
                    frameRectij.draw(g);
                }
        }

        for (RectFrame compRect : compRects) {
            if (compRect != null) {
                compRect.draw(g);
            }
        }

        for (RectFrame compSpikeRect : compSpikeRects) {
            if (compSpikeRect != null) {
                compSpikeRect.draw(g);
            }
        }

        for (RectFrame playerRect : playerRects) {
            if (playerRect != null)
                playerRect.draw(g);
        }
    }

}
