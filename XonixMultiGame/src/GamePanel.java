
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Objects;
import java.util.Random;
import javax.swing.*;

public class GamePanel extends JPanel {
	public GameScreenshotFactory gff;

	//private Ball b1,b2;
	private JFrame window;
	public static volatile boolean isMsgDisplayed = false;

	public static final int height = 800 - 14;
	public static final int width = 800 - 37;

	//private int[] score = new int[]{0, 0}, lives = new int[]{3, 3}; //  Integer.MAX_VALUE, Integer.MAX_VALUE
	private User[] users; // = new User[2];
	private int level;
	private double precents = 0;

	private CompMovingRect[] compRects;
	public static int COMP_COUNT = 5;

	private CompSpikeMovingRect[] compSpikeRects;
	public static int COMP_SPIKE_COUNT = 2;

	private Rect[][] frameRects;
	private PlayerRect[] playerRects;

	private Stats stats;
	private SoundEffects sfx;

	private volatile boolean isPaused = false;


	public void createFrame() {
		frameRects = new Rect[height / 20][width / 20];
		System.out.println("H: " + frameRects.length + " | W: " + frameRects[0].length);
		int rWidth = 20;

		for (int i = 0; i < frameRects.length; i++) {
			for (int j = 0; j < frameRects[i].length; j++) {
				Color c = Color.GRAY;
				if (i == 0 || j == 0 || i == frameRects.length - 1 || j == frameRects[i].length - 1) c = Color.BLUE;
				frameRects[i][j] = new Rect(20 * i, 20 * j, rWidth, rWidth, c, this);
			}
		}

		System.out.println(frameRects[frameRects.length - 1][frameRects[0].length - 1].getX() + " " + frameRects[frameRects.length - 1][frameRects[0].length - 1].getY());

	}

	public void createComps() {
		compRects = new CompMovingRect[COMP_COUNT];
		int rWidth = 15;
		Rect other;
		Random r = new Random();

		for (int i = 0; i < compRects.length; i++) {
			do {
				other = getFrameRectAtij(r.ints(0, getFrameRectsiLen()).findFirst().getAsInt(), r.ints(0, getFrameRectsjLen()).findFirst().getAsInt());
			} while (other.getColor() != Color.GRAY);
			compRects[i] = new CompMovingRect(other.getX(), other.getY(), rWidth, rWidth, Color.RED, this);//new CompMovingRect(r.ints(rWidth*2 + 10, height - rWidth*2 - rWidth*2).findFirst().getAsInt() + i * rWidth, r.ints(rWidth*2 + 10, width - rWidth*2 - rWidth*2).findFirst().getAsInt() + i * rWidth, rWidth, rWidth, Color.red, this);
			compRects[i].start();
		}
	}

	public void createSpikeComps() {
		compSpikeRects = new CompSpikeMovingRect[COMP_SPIKE_COUNT];
		int rWidth = 15;
		Rect other;
		Random r = new Random();

		for (int i = 0; i < compSpikeRects.length; i++) {
			do {
				other = getFrameRectAtij(r.ints(0, getFrameRectsiLen()).findFirst().getAsInt(), r.ints(0, getFrameRectsjLen()).findFirst().getAsInt());
			} while (other.getColor() != Color.BLUE);

			compSpikeRects[i] = new CompSpikeMovingRect(other.getX(), other.getY(), rWidth, rWidth, Color.GREEN, this); //new CompSpikeMovingRect(r.ints(width - rWidth, width).findFirst().getAsInt() + i * rWidth, r.ints(0, rWidth).findFirst().getAsInt() + i * rWidth, rWidth, rWidth, Color.green, this);
			compSpikeRects[i].start();
		}
	}

	public void createPlayer(int i) {

		int rWidth = 20;
		Color c = null;
		if (i == 0)
			c = Color.ORANGE;
		else if (i == 1)
			c = Color.YELLOW;
		playerRects[i] = new PlayerRect(0, 0, rWidth, rWidth, c, this, i);
		playerRects[i].start();
	}

	public void createPlayers() {
		playerRects = new PlayerRect[2];
		for (int i = 0; i < playerRects.length; i++) {
			createPlayer(i);
		}
	}

	public void createUsers() {
		for (int i = 0 ; i < users.length; i++) {
			//users[i] = new User(i, this);
			users[i].setPanel(this);
			users[i].resetLifes();
		}
	}


	public GamePanel(JFrame window, User[] users, int level) {
		//b1=new Ball(56,77,50,Color.RED,this);
		//b2=new Ball(123,212,60,Color.BLUE,this);

		//r1 = new MovingRect(56, 77, 10, 10, Color.RED, this);
		this.level = level;
		this.users = users;
		this.window = window;

		createFrame();
		createComps();
		//createPlayer();
		createPlayers();
		createUsers();
		createSpikeComps();

		stats = new Stats(GamePanel.width, GamePanel.height, 10, 10, this);
		stats.start();
		sfx = new SoundEffects(this);
		sfx.start();
		gff = new GameScreenshotFactory(window, this);


		//b1.start();
		//b2.start();
		setBackground(Color.GRAY);
		setFocusable(true);
		addKeyListener(new P1_KL());
		addKeyListener(new P2_KL());
		addKeyListener(new Screenshot());
		addKeyListener(new pause_resume());
		System.out.println("Created GP");
	}

	public Rect getCompRectAti(int i) {
		return compRects[i];
	}

	public int getCompRectsLen() {
		return compRects.length;
	}

	public boolean hasCompAtXY(int x, int y) {
		for (int i = 0; i < compRects.length; i++) {
			//System.out.println(x + " " + y + " | " + compRects[i].x + " " + compRects[i].y);
			if (compRects[i].x == x && compRects[i].y == y)
				return true;

		}

		return false;
	}

	public Rect getSpikeCompRectAti(int i) {
		return compSpikeRects[i];
	}

	public boolean hasSpikeAtXY(int x, int y) {
		for (int i = 0; i < compSpikeRects.length; i++) {
			if (compSpikeRects[i].x == x && compSpikeRects[i].y == y)
				return true;
		}

		return false;
	}

	public int getSpikeCompRectsLen() {
		return compSpikeRects != null ? compSpikeRects.length : 0;
	}

	public int getFrameRectsiLen() {
		return this.frameRects.length;
	}

	public int getFrameRectsjLen() {
		return this.frameRects[0].length;
	}

	public Rect getFrameRectAtij(int i, int j) {
		return this.frameRects[i][j];
	}

	public int getPlayerRectsLen() {
		return playerRects != null ? playerRects.length : 0;
	}

	public Rect getPlayerRectAti(int i) {
		return playerRects[i];
	}

	public int getScore(int i) {

		//return score[i];
		return users[i].getScore();
	}

	public int getLives(int i) {

		//return lives[i];
		return users[i].getLifes();
	}

	/*public User[] getUsers() {
		return users;
	}*/

	public User getUserAti(int i) {
		return this.users[i];
	}

	public int getUsersLen() {
		return this.users.length;
	}

	public CompMovingRect[] getCompRects() {
		return compRects;
	}

	public CompSpikeMovingRect[] getCompSpikeRects() {
		return compSpikeRects;
	}

	public Rect[][] getFrameRects() {
		return frameRects;
	}

	public PlayerRect[] getPlayerRects() {
		return playerRects;
	}

	public Stats getStats() {
		return stats;
	}

	public double getPrecents() {
		int blueCount = 0;

		for (int i = 0; i < frameRects.length; i++) {
			for (int j = 0; j < frameRects[0].length; j++) {
				if (frameRects[i][j].getColor() == Color.BLUE)
					blueCount++;
			}
		}

		//System.out.println(blueCount + " / " + (frameRects.length * frameRects[0].length));

		this.precents = (blueCount / (frameRects.length * frameRects[0].length * 1.0)) * 100;
		return this.precents;
	}

	public int getLevel() {
		return level;
	}

	public boolean isPaused() {
		return isPaused;
	}

	public void setPaused(boolean paused) {
		isPaused = paused;
	}

	public void lostLive(int i) {
		users[i].removeLife(); //lives[i]--;
		if (users[i].getLifes() > 0)
			createPlayer(i);
	}

	public void incScore(int score, int i) {
		//this.score[i] += score;
		if (users[i] != null)
			users[i].addScore(score);
	}

	/*public Ball getB1(){
                return b1;
            }
            public Ball getB2(){
                return b2;
            }*/

	public void stopBG() {
		sfx.interrupt();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);


		for (int i = 0; i < frameRects.length; i++) {
			for (int j = 0; j < frameRects[0].length; j++)
				if (frameRects[i][j] != null) {
					frameRects[i][j].draw(g);
				}
		}

		for (int i = 0; i < compRects.length; i++) {
			if (compRects[i] != null) {
				compRects[i].draw(g);
			}
		}

		for (int i = 0; i < compSpikeRects.length; i++) {
			if (compSpikeRects[i] != null) {
				compSpikeRects[i].draw(g);
			}
		}

		for (int i = 0; i < playerRects.length; i++) {
			if (playerRects[i] != null)
				playerRects[i].draw(g);
			else
				createPlayer(i);
		}

		getPrecents();

		//height = this.getHeight();
		//width = this.getWidth();

		//b2.draw(g);
		//b1.draw(g);

		/*int[] xPoints = {100,50,150};
		int[] yPoints = {100,200,200};

		//g.setColor(Color.black);
		//g.drawPolygon(xPoints, yPoints, 3);
		g.setColor(Color.red);
		g.fillPolygon(xPoints, yPoints, 3);*/
	}

	class P1_KL extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			int code = e.getKeyCode();

			if (!isPaused) {
				if (code == KeyEvent.VK_RIGHT) {
					playerRects[0].setRight();
					//System.out.println("R");
				} else if (code == KeyEvent.VK_LEFT) {
					playerRects[0].setLeft();
					//System.out.println("L");
				} else if (code == KeyEvent.VK_UP) {
					playerRects[0].setUp();
					//System.out.println("U");
				} else if (code == KeyEvent.VK_DOWN) {
					playerRects[0].setDown();
					//System.out.println("D");
				}

				repaint();
			}

		}
	}

	class P2_KL extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			int code = e.getKeyCode();

			if (!isPaused) {
				if (code == KeyEvent.VK_D) {
					playerRects[1].setRight();
					//System.out.println("R");
				} else if (code == KeyEvent.VK_A) {
					playerRects[1].setLeft();
					//System.out.println("L");
				} else if (code == KeyEvent.VK_W) {
					playerRects[1].setUp();
					//System.out.println("U");
				} else if (code == KeyEvent.VK_S) {
					playerRects[1].setDown();
					//System.out.println("D");
				}

				repaint();
			}

		}
	}

	public void controlPlayerati(int i, String dir) {
		if (!isPaused) {
			if (dir.equals("RIGHT")) {
				playerRects[i].setRight();
			} else if (dir.equals("LEFT")) {
				playerRects[i].setLeft();
			} else if (dir.equals("UP")) {
				playerRects[i].setUp();
			} else if (dir.equals("DOWN")) {
				playerRects[i].setDown();
			}
		}
	}

	public void takeScreenshotNow() {
		gff.takeScreenshotNow();
	}

	class Screenshot extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			int code = e.getKeyCode();

			if (code == KeyEvent.VK_T) {
				takeScreenshotNow();
			}

			repaint();

		}
	}

	public void resumeGame() {
		for (int i = 0; i < compRects.length; i++) {
			synchronized (compRects[i]) {
				compRects[i].notify();
			}
		}

		for (int i = 0; i < compSpikeRects.length; i++) {
			synchronized (compSpikeRects[i]) {
				compSpikeRects[i].notify();
			}
		}

		for (int i = 0; i < playerRects.length; i++) {
			synchronized (playerRects[i]) {
				playerRects[i].notify();
			}
		}
		isPaused = false;
	}

	public void pauseGame() {
		isPaused = true;

	}

	class pause_resume extends KeyAdapter {
		public synchronized void keyPressed(KeyEvent e) {
			int code = e.getKeyCode();

			if (code == KeyEvent.VK_R) {
				/*for (int i = 0; i < compRects.length; i++) {
					synchronized (compRects[i]) {
						compRects[i].notify();
					}
				}

				for (int i = 0; i < compSpikeRects.length; i++) {
					synchronized (compSpikeRects[i]) {
						compSpikeRects[i].notify();
					}
				}

				for (int i = 0; i < playerRects.length; i++) {
					synchronized (playerRects[i]) {
						playerRects[i].notify();
					}
				}
				isPaused = false;
				System.out.println(isPaused);*/
				resumeGame();
				System.out.println(isPaused);
			}
			if (code == KeyEvent.VK_P) {
				/*isPaused = true;
				System.out.println(isPaused);*/
				pauseGame();
				System.out.println(isPaused);
			}

		}
	}

/*		public static void main(String[] args) {
			JFrame f = new JFrame("Xonix Multiplayer by DZSH");
			GamePanel gp = new GamePanel(f);
			f.add(gp);
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f.setSize(800, 800);
			f.setResizable(false);
			f.setVisible(true);
			f.setFocusable(false);
			System.out.println("H: " + gp.getHeight() + " | W: " + gp.getWidth());
		}*/

	}