import javax.swing.*;
import java.awt.*;
import java.io.Serializable;
import java.text.NumberFormat;

public class Stats extends Thread implements Serializable {

    //int[] score = new int[]{0, 0};
    //int[] life = new int[]{3, 3};
    double precents = 0;
    int lvl;

    JLabel pausedLabel, precentLabel, lvlLabel;
    //JLabel[] lifeLabels, scoreLabels;
    JLabel[] playerLabels;

    volatile GamePanel panel;
    int x, y, w, h;

    public Stats(int x, int y, int w, int h, GamePanel panel) {

        //this.scoreLabels = new JLabel[panel.getPlayerRectsLen()];
        // this.lifeLabels = new JLabel[panel.getPlayerRectsLen()];
        this.panel = panel;
        this.lvl = panel.getLevel();
        this.playerLabels = new JLabel[panel.getPlayerRectsLen()];

        /*for (int i = 0; i < panel.getPlayerRectsLen(); i++) {
            this.playerLabels[i] = new JLabel("P" + (i + 1) + ": " + score[i] + "pts, " + life[i] + "♥ |", SwingConstants.CENTER);
            //this.scoreLabels[i] = new JLabel("P" + (i + 1) +"'s Score: " + score[i], SwingConstants.RIGHT);
            //this.lifeLabels[i] = new JLabel("P" + (i + 1) + "'s Lifes: " + life[i], SwingConstants.LEFT);
            panel.add(this.playerLabels[i]);
            playerLabels[i].setForeground(panel.getPlayerRectAti(i).getColor());
        }*/

        for (int i = 0; i < panel.getUsersLen(); i++) {
            panel.add(panel.getUserAti(i).getLabel());
            panel.getUserAti(i).setup();
        }

        this.precentLabel = new JLabel(precents + "% |", SwingConstants.LEFT);
        this.lvlLabel = new JLabel("Lv." + lvl + " |");
        this.pausedLabel = new JLabel();


        /*for (int i = 0; i < lifeLabels.length; i++)
            this.panel.add(this.lifeLabels[i]);

        for (int i = 0; i < scoreLabels.length; i++)
            this.panel.add(this.scoreLabels[i]);*/

        this.panel.add(this.precentLabel);
        this.panel.add(this.lvlLabel);
        this.panel.add(this.pausedLabel);

        /*for (int i = 0; i < scoreLabels.length; i++)
            scoreLabels[i].setForeground(Color.WHITE);

        for (int i = 0; i < lifeLabels.length; i++)
            lifeLabels[i].setForeground(Color.WHITE);*/

        precentLabel.setForeground(Color.WHITE);
        lvlLabel.setForeground(Color.WHITE);
        pausedLabel.setForeground(Color.WHITE);

        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        //this.lifeLabel.setBounds(x / 2, y, w, h);
        //this.scoreLabel.setBounds(x, y, w, h);
    }

    public void draw(Graphics g) {
        for (int i = 0; i < panel.getUsersLen(); i++) {
            //this.playerLabels[i].setBounds(x, y, w, h);
            //this.playerLabels[i].setText("P" + (i + 1) + ": " + score[i] + "pts, ♥" + life[i] + " |");
        }

        /*for (int i = 0; i < scoreLabels.length; i++) {
            this.scoreLabels[i].setBounds(x, y, w, h);
            this.scoreLabels[i].setText("P" + (i + 1) +"'s Score: " + score[i]);
        }

        for (int i = 0; i < lifeLabels.length; i++) {
            this.lifeLabels[i].setBounds(x, y, w, h);
            this.lifeLabels[i] = new JLabel("P" + (i + 1) + "'s Lifes: " + life[i], SwingConstants.LEFT);
        }*/

        this.precentLabel.setBounds(x, y, w, h);
        this.precentLabel.setText(String.format("%3.2f %% |", precents));
        //g.drawString(this.scoreLabel.getText(), x, y);
    }

    public void run() {
        while (true) {

            /*for (int i = 0; i < panel.getPlayerRectsLen(); i++)
                this.score[i] = this.panel.getScore(i);*/

            /*for (int i = 0; i < panel.getPlayerRectsLen(); i++)
                this.life[i] = this.panel.getLives(i);*/

            for (int i = 0; i < panel.getUsersLen(); i++) {
                panel.getUserAti(i).getLabel();
            }

            this.precents = this.panel.getPrecents();
            //System.out.println(this.score);

            /*
            for (int i = 0; i < scoreLabels.length; i++)
                this.scoreLabels[i].setText("P" + (i + 1) +"'s Score: " + score[i]);

            for (int i = 0; i < lifeLabels.length; i++)
                this.lifeLabels[i].setText("P" + (i + 1) + "'s Lifes: " + (life[i] > 3 ? "∞" : life[i]));
            */

            /*for (int i = 0; i < panel.getPlayerRectsLen(); i++) {
                this.playerLabels[i].setText("P" + (i + 1) + ": " + score[i] + "pts, " + life[i] + "♥ |");
            }*/

            this.precentLabel.setText(String.format("%3.3f %% |", precents));
            //this.scoreLabel.repaint();

            this.pausedLabel.setText(panel.isPaused() ? " PAUSED" : "");

            int lifeSum = 0;
            for (int i = 0; i < this.panel.getPlayerRectsLen(); i++) {
                lifeSum += this.panel.getLives(i);
            }

            if ((lifeSum == 0 || precents >= 90) && !GamePanel.isMsgDisplayed) {
                String scoresMsg = "";
                int maxScore = 0, maxIndex = 0;

                /*for (int i = 0; i < score.length; i++) {
                    scoresMsg += "P" + (i + 1) + ": " + score[i] + "\n";
                    if (score[i] >= maxScore) {
                        maxScore = score[i];
                        maxIndex = i;
                    }
                }*/

                for (int i = 0; i < panel.getUsersLen(); i++) {
                    scoresMsg += (panel.getUserAti(i).getUsername().isBlank() ? "P" + (i + 1) : panel.getUserAti(i).getUsername()) + ": " + panel.getUserAti(i).getScore() + "\n";
                    if (panel.getUserAti(i).getScore() >= maxScore) {
                        maxScore = panel.getUserAti(i).getScore();
                        maxIndex = i;
                    }
                }

                String winCond = lifeSum == 0 ? "Game Over\n" : precents >= 90 ? "Reached Goal\n"  : "";

                this.panel.stopBG();
                if (lifeSum == 0) SoundEffects.playLose();
                else if (precents >= 90) SoundEffects.playWin();


                JOptionPane.showMessageDialog(null, winCond + (panel.getUserAti(maxIndex).getUsername().isBlank() ? "P" + (maxIndex + 1) : panel.getUserAti(maxIndex).getUsername()) + " Won!\n" + scoresMsg);
                GamePanel.isMsgDisplayed = true;
                System.out.println("MSG DISPLAYED");
                //System.exit(0);
                break;
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {}
        }
    }

}