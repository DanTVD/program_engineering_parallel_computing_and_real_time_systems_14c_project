import java.awt.*;
import java.io.Serializable;

public class CompMovingRect extends Rect implements Serializable {
    private static int id_count = 0;
    private int id;
    //private Point p;
    protected static int DELAY = 5;

    public CompMovingRect(int x, int y, int width, int height, Color color, GamePanel p) {
        super(x, y, width, height, color, p);
        this.id = id_count++;
        // this.delay = 5;
        //this.p = new Point(1,2);
    }

    public void run() {
        //super.run();
        dirx = 1;
        diry = 1;
        //double delta = 0.5;
        int wtfCount = 0;
        //Random rnd = new Random();

        double t = 0;
        while (true) {
            int h = this.getPanel().getHeight();
            int w = this.getPanel().getWidth();
            boolean isHit = false, isFrameHitted = false;
            boolean isPaused = getPanel().isPaused();

            /*if (getX() + getWidth() / 2 > w)
                dirx = -1;

            if (getX() - getWidth() / 2 < 0)
                dirx *= -1;//= 1;

            if (getY() + getWidth() / 2 > h)
                diry = -1;

            if (getY() - getWidth() / 2 < 0)
                diry *= -1; //= 1; */

            Rect other = null;

            for (int i = 0; i < this.getPanel().getFrameRectsiLen() && !isHit; i++) {
                for (int j = 0; j < this.getPanel().getFrameRectsjLen() && !isHit; j++) {
                    other = this.getPanel().getFrameRectAtij(i, j);

                    if (other != null && this.intersects(other) && other.getColor() != Color.GRAY && ((other.getColor() != PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P1))) && (other.getColor() != PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P2))))) { //Color.LIGHT_GRAY

                        if (t % 2 == 0) {
                            dirx *= -1;
                        }
                        else {
                            diry *= -1;
                        }

                        /*if(this.x + this.width > other.x && this.x < other.x) { // + other.width
                            diry = (diry * 2 + 1) % 2 * rnd.ints(1, DELAY).findFirst().getAsInt();
                            dirx = DELAY - Math.abs(diry);
                        }
                        if (this.x < other.x + other.width && other.x)*/

                        /*if (i == 0) { // Hits Up
                            System.out.println(i);
                            dirx = (int) (-1 * (1 - delta) * dirx + p.x);
                            diry = (int) (-1 * (1 + delta) * diry + p.y);

                            this.delay *= 2;
                        }
                        if (i == this.getPanel().getFrameRectsiLen() - 1) {

                        }
                        if (j == 0) {
                            System.out.println(j);
                            dirx = (int) (-1 * (1 + delta) * dirx + p.x);
                            diry = (int) (-1 * (1 - delta) * diry + p.y);

                            if (delay / 1.5 != 0) this.delay /= 1.5;
                        }
                        if (j == this.getPanel().getFrameRectsjLen() - 1) {

                        }*/

                        /*if (t % 2 == 0) {
                            dirx = (int) ((-0.9 * p.x) * dirx + p.x); //-1
                            diry = (int) ((-0.9 * p.y) * diry + p.y); //-1

                            this.delay *= 2;
                        }
                        else {
                            dirx = (int) (-1.1 * dirx + p.x);
                            diry = (int) (-1.1 * dirx + p.x);
                            if (delay / 1.5 != 0) this.delay /= 1.5;
                        }*/


                        //t=t+50;

                        wtfCount++;
                        //System.out.println("Rect #" + this.id + " Hitted " + wtfCount + " | " + t);
                        isHit = true;
                        isFrameHitted = true;
                    }
                }
            }

            for (int i = 0; i < this.getPanel().getCompRectsLen() && !isHit; i++) {
                other = this.getPanel().getCompRectAti(i);

                if (other != null && other != this && this.intersects(other) && other.getColor() != Color.GRAY && ((other.getColor() != PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P1))) && (other.getColor() != PlayerRect.P_Trails.get((PlayerRect.PlayerTrail.P2))))) { //Color.LIGHT_GRAY

                    if (t % 2 == 0) {
                        dirx *= -1;
                    }
                    else {
                        diry *= -1;
                    }

                    wtfCount++;
                    //System.out.println("Rect #" + this.id + " Hitted " + wtfCount + " | " + t);
                    isHit = true;
                }
            }

            for (int i = 0; i < this.getPanel().getPlayerRectsLen(); i++) {
                other = this.getPanel().getPlayerRectAti(i);

                if (!isFrameHitted && other != null && this.intersects(other) && other.getColor() != Color.GRAY) {

                    if (t % 2 == 0) {
                        dirx *= -1;
                    } else {
                        diry *= -1;
                    }

                    wtfCount++;
                    //System.out.println("Rect #" + this.id + " Hitted " + wtfCount + " | " + t);
                    isHit = true;
                }
            }

            //System.out.println("Aight imma head out");

            setX(getX() + dirx);
            setY(getY() + diry);
            t++;

            if (isPaused) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.getPanel().repaint();
        }
    }


}
