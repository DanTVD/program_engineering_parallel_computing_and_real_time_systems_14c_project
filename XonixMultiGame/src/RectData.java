import java.awt.*;
import java.io.Serializable;

public class RectData implements Serializable {
    protected volatile int x, y, width, height;
    private Color color;

    RectData(int x, int y, int width, int height, Color color) {
        this.setX(x);
        this.setY(y);
        this.setWidth(width);
        this.setHeight(height);
        this.color=color;
    }

    public void draw(Graphics g){
        g.setColor(color);
        //g.fillOval(x-width/2,y-width/2,width,width);
        g.fillRect(getX(),getY(),getWidth(),getHeight());
        //g.setColor(Color.BLACK);
        g.drawRect(getX(), getY(), getWidth(), getHeight());
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Color getColor() {
        return this.color;
    }
}
