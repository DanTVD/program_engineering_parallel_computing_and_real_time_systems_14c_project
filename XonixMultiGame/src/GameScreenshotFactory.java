import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class GameScreenshotFactory extends Thread implements Serializable {
    JFrame frame;
    GamePanel panel;

    public GameScreenshotFactory(JFrame frame, GamePanel panel) {
        this.frame = frame;
        this.panel = panel;
    }

    public void takeScreenshot(String path) {

        Container c = frame.getContentPane();
        BufferedImage im = new BufferedImage(c.getWidth(), c.getHeight(), BufferedImage.TYPE_INT_ARGB);
        c.paint(im.getGraphics());
        try {
            ImageIO.write(im, "PNG", new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void takeScreenshotNow() {
        takeScreenshot("Screenshots/" + Date.getDate() + ".png");
    }

    /*@Override
    public void run() {

        while (true) {
            try {
                takeScreenshot("Frames/" + Date.getDate() + ".png");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }

            Date.clearUnusedDates(Folder.getFolderFileNames("Frames/"));

            try {
                sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }*/
}
