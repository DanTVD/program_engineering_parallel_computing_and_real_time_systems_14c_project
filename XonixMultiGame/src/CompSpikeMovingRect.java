import java.awt.*;

public class CompSpikeMovingRect extends CompMovingRect{
    public CompSpikeMovingRect(int x, int y, int width, int height, Color color, GamePanel p) {
        super(x, y, width, height, color, p);
    }

    public void run() {
        double t = 0;

        dirx = 1;
        diry = 1;
        while (true) {
            int h = this.getPanel().getHeight();
            int w = this.getPanel().getWidth();
            boolean isHit = false, isFrameHitted = false;
            boolean isPaused = getPanel().isPaused();

            if (getX() + getWidth() / 2 > w)
                dirx = -1;

            if (getX() - getWidth() / 2 < 0)
                dirx = 1;

            if (getY() + getWidth() / 2 > h)
                diry = -1;

            if (getY() - getWidth() / 2 < 0)
                diry = 1;

            Rect other;

            for (int i = 0; i < this.getPanel().getFrameRectsiLen() && !isHit; i++) {
                for (int j = 0; j < this.getPanel().getFrameRectsjLen() && !isHit; j++) {
                    other = this.getPanel().getFrameRectAtij(i, j);

                    if (other != null && this.intersects(other) && other.getColor() != Color.BLUE) {

                        if (t % 2 == 0) {
                            dirx *= -1;
                        }
                        else {
                            diry *= -1;
                        }

                        //System.out.println("Rect #" + this.id + " Hitted " + wtfCount + " | " + t);
                        isHit = true;
                        isFrameHitted = true;
                    }
                }
            }

            for (int i = 0; i < this.getPanel().getSpikeCompRectsLen() && !isHit; i++) {
                other = this.getPanel().getSpikeCompRectAti(i);

                if (other != null && other != this && this.intersects(other) && other.getColor() != Color.BLUE) {

                    if (t % 2 == 0) {
                        dirx *= -1;
                    }
                    else {
                        diry *= -1;
                    }


                    //System.out.println("Rect #" + this.id + " Hitted " + wtfCount + " | " + t);
                    isHit = true;
                }
            }

            for (int i = 0; i < this.getPanel().getPlayerRectsLen(); i++) {

                other = this.getPanel().getPlayerRectAti(i);

                if (!isFrameHitted && other != null && this.intersects(other) && other.getColor() != Color.GRAY) {

                    if (t % 2 == 0) {
                        dirx *= -1;
                    }
                    else {
                        diry *= -1;
                    }


                    //System.out.println("Rect #" + this.id + " Hitted " + wtfCount + " | " + t);
                    isHit = true;
                }

            }

            //System.out.println("Aight imma head out");

            setX(getX() + dirx);
            setY(getY() + diry);
            t++;

            if (isPaused) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.getPanel().repaint();
        }
    }
}
