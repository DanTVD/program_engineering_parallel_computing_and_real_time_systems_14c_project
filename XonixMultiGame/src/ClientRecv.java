import javax.swing.*;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.concurrent.Semaphore;

public class ClientRecv extends Thread {
    private Socket socket;
    private Semaphore q_mtx;
    private ClientEnd clientEnd;

    public ClientRecv(Socket socket, Semaphore q_mtx, ClientEnd clientEnd) {
        this.socket = socket;
        this.q_mtx = q_mtx;
        this.clientEnd = clientEnd;
    }

    private String recvFromServer() {
        try {

            DataInputStream input = new DataInputStream(socket.getInputStream());
            String string = input.readUTF();
            //System.out.println("Server: " + string);
            return string;
        } catch (Exception ev) {
            System.out.println("Network Problem ");

            try {
                socket.close();
                sleep(5000);
                System.exit(0);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public JLabel recvFromServerLabel() {
        try {
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());

            return (JLabel) input.readObject();
        } catch (Exception ev) {
            ev.printStackTrace();
            System.out.println("Network Problem ");

            try {
                socket.close();
                sleep(5000);
                System.exit(0);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void run() {
        String msg;
        //JLabel label;

        while (true) {
            try {
                q_mtx.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            msg = recvFromServer();
            System.out.println(msg);
            clientEnd.q.add(msg);

            //if (msg.equals("INFO")) {
                //label = recvFromServerLabel();
                //System.out.println(label.getText());
            //}

            q_mtx.release();
            System.out.println("msg added to q");

            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
