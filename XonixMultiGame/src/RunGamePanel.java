import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class RunGamePanel extends Thread {
    private JFrame f;
    private GamePanel gp;
    private User[] users;
    private int lvl;

    private JFrame MenuF;

    RunGamePanel(JFrame MenuF) {
        f = new JFrame("Xonix Multiplayer by DZSH");
        lvl = 1;
        CompMovingRect.DELAY = 5;
        GamePanel.COMP_COUNT = 5;
        GamePanel.COMP_SPIKE_COUNT = 2;

        createUsersGlobal();
        this.MenuF = MenuF;

        gp = new GamePanel(f, users, lvl);
        f.add(gp);
        //f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                MenuF.setVisible(true);
            }

            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                MenuF.setVisible(false);
            }
        });
        f.setSize(800, 800);
        f.setResizable(false);
        f.setVisible(true);
        f.setFocusable(false);
        System.out.println("H: " + gp.getHeight() + " | W: " + gp.getWidth());
    }

    private void createUsersGlobal() {
        this.users = new User[2];

        for (int i = 0; i < users.length; i++) {
            users[i] = new User(i);
        }
    }

    public GamePanel getGp() {
        return gp;
    }

    public JLabel getLabelUserAti(int i) {
        return gp.getUserAti(i).getLabel();
    }

    private boolean incLv() {
        ++lvl;

        //if (lvl == 6) System.exit(0);

        if (CompMovingRect.DELAY > 1 && lvl % 2 == 0) {
            CompMovingRect.DELAY--;
            System.out.println(CompMovingRect.DELAY);
        }
        if (CompMovingRect.DELAY == 1) {
            JOptionPane.showMessageDialog(null, "End Of Game\n\nReached Last Level");

            ScoreTable ST = new ScoreTable(f, MenuF);
            ST.insertScore(users[0], users[1]);
            ST.setScoresatTable();
            ST.closeDB();
            //System.exit(0);
            return false;
        }

        if (lvl % 2 == 0) GamePanel.COMP_COUNT += 2;
        if (lvl % 3 == 0) GamePanel.COMP_SPIKE_COUNT += 1;
        return true;
    }

    @Override
    public void run() {
        boolean running = true;
        while (running) {
            if (GamePanel.isMsgDisplayed) {
                f.remove(gp);
                GamePanel.isMsgDisplayed = false;
                if (incLv()) {
                    gp = new GamePanel(f, users, lvl);
                    f.add(gp);
                    f.setResizable(false);
                    f.setVisible(true);
                    f.setFocusable(false);
                    System.out.println("H: " + gp.getHeight() + " | W: " + gp.getWidth());
                } else running = false;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.gp.repaint();
        }
    }


}
