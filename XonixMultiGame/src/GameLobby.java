import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static java.lang.Thread.sleep;

public class GameLobby extends Thread {
    private ImageIcon loadingIcon;
    private JLabel loadingLbl;

    private JFrame f;
    private JPanel panel;

    public GameLobby(JFrame MenuF) {
        f = new JFrame("Xonix Multiplayer Lobby by DZSH");
        panel = new JPanel();
        panel.setLayout(new BorderLayout());

        loadingIcon = new ImageIcon("./gifs/loading.gif");
        loadingLbl = new JLabel("Loading...", loadingIcon, JLabel.CENTER);

        panel.add(loadingLbl, BorderLayout.CENTER);

        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                MenuF.setVisible(true);
            }

            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                MenuF.setVisible(false);
            }
        });
        f.add(panel);
        f.setSize(500, 400);
        f.setResizable(false);
        f.setVisible(true);
        f.setFocusable(false);


    }



    @Override
    public void run() {
        while (!ClientHandler.isGameReady()) {
            loadingLbl.setText("Loading.");
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            loadingLbl.setText("Loading..");
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            loadingLbl.setText("Loading...");
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //f.dispose();
    }
}
