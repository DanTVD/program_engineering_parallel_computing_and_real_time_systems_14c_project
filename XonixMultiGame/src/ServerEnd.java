// 2-10-2021 MS
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;

import static java.lang.Thread.sleep;

public class ServerEnd extends JPanel
{
	static int port = 6000;
	
	private ServerSocket server ;
	private Socket socket;
	//private RunGamePanel rungp;
	private ClientHandler[] clients;

	private JFrame f;
	private ServerMenu sm;
	
	public ServerEnd() throws IOException 
	{

		f = new JFrame("Xonix Multiplayer Menu by DZSH");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(500, 400);

		sm = new ServerMenu(f, this);

		this.f.add(sm);
		f.setResizable(false);
		f.setVisible(true);
		f.setFocusable(false);

	}

	public void serverConnection() throws IOException {
		 server = new ServerSocket(port);

		 clients = new ClientHandler[2];

		 for (int i = 0; i < clients.length; i++) {
			 socket = server.accept();
			 System.out.println("Client found !");
			 while (sm.getRungp() == null) {
				 System.out.println("Game is not Online yet");
				 try {
					 sleep(500);
				 } catch (InterruptedException e) {
					 e.printStackTrace();
				 }
			 }
			 clients[i] = new ClientHandler(socket, sm.getRungp());
			 clients[i].start();
		 }

	}

	public void updateRGPforClientHandlers(RunGamePanel rgp) {
		for (ClientHandler i : clients) {
			if (i != null) // first time is null since clients werent accepted yet
				i.setRgp(rgp);
		}
	}


	public static void main(String[] args) throws IOException 
	{
		/*for (Method m : GamePanel.class.getDeclaredMethods()) {
			System.out.println(m);
		}*/

		ServerEnd s = new ServerEnd();
		s.serverConnection();

	}
}
