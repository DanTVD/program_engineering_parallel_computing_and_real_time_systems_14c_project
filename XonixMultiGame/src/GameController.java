import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import static java.lang.Thread.sleep;

public class GameController extends JPanel implements ActionListener{
    private JButton[][] keys;

    private JTextField username;
    private JButton submitUsername;

    private JLabel info;

    private JButton resume, pause;

    private JButton screenshot;

    private Socket socket;
    private int id;

    public GameController(Socket socket, int id) {

        this.socket = socket;
        this.id = id;

        this.setLayout(new GridLayout(4, 3));

        this.setBackground(Color.white);

        keys = new JButton[2][3];
        keys[0][1] = new JButton("⬆");
        keys[1][0] = new JButton("⬅");
        keys[1][1] = new JButton("⬇");
        keys[1][2] = new JButton("➡");

        username = new JTextField("");
        submitUsername = new JButton("Submit \nUsername");
        info = new JLabel("");

        resume = new JButton("▶");
        pause = new JButton("⏸");

        screenshot = new JButton("📸");

        this.add(username);
        this.add(submitUsername);
        this.add(info);

        this.add(resume);
        this.add(pause);
        this.add(screenshot);

        submitUsername.addActionListener(this);
        resume.addActionListener(this);
        pause.addActionListener(this);
        screenshot.addActionListener(this);

        //setNoBGbtnPartly(submitUsername);
        setNoBGbtnPartly(resume);
        setNoBGbtnPartly(pause);
        setNoBGbtnPartly(screenshot);


        for (int i = 0; i < keys.length; i++) {
            for (int j = 0; j < keys[i].length; j++) {
                if (keys[i][j] != null) {
                    this.add(keys[i][j]);
                    keys[i][j].addActionListener(this);
                    keys[i][j].addKeyListener(new P_Ctrls());
                    //setNoBGbtnPartly(keys[i][j]);
                }
                else {
                    JButton empty = new JButton("");
                    empty.setBackground(Color.white);
                    setNoBGbtnFully(empty);
                    this.add(empty);

                }
            }
        }


        this.addKeyListener(new P_Ctrls());
        this.setFocusable(true);
    }

    private void sendToServer(String msg) {
        //System.out.println("Client : ");
        try {
            DataOutputStream output = new DataOutputStream(socket.getOutputStream());
            output.writeUTF(msg);
        } catch (IOException e1) {
            System.out.println("Network issues");
            try {
                sleep(2000);
                System.exit(0);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }

        }
    }

    public void setInfo(String info) {
        this.info.setText(info);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == keys[0][1]) {
            System.out.println("UP");
            sendToServer(id + " UP");
        } else if (e.getSource() == keys[1][0]) {
            System.out.println("LEFT");
            sendToServer(id + " LEFT");
        } else if (e.getSource() == keys[1][1]) {
            System.out.println("DOWN");
            sendToServer(id + " DOWN");
        } else if (e.getSource() == keys[1][2]) {
            System.out.println("RIGHT");
            sendToServer(id + " RIGHT");
        } else if (e.getSource() == this) {
            requestFocusInWindow();
        } else if (e.getSource() == submitUsername) {
            submitUsername.setEnabled(false);
            username.setEnabled(false);
            sendToServer(id + " USERNAME " + username.getText());
        } else if (e.getSource() == resume) {
            sendToServer(id + " RESUME");
        } else if (e.getSource() == pause) {
            sendToServer(id + " PAUSE");
        } else if (e.getSource() == screenshot) {
            sendToServer(id + " SCREENSHOT");
        }
    }

    class P_Ctrls extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            int code = e.getKeyCode();

            if (code == KeyEvent.VK_UP) {
                System.out.println("UP");
                sendToServer(id + " UP");
            } else if (code == KeyEvent.VK_LEFT) {
                System.out.println("LEFT");
                sendToServer(id + " LEFT");
            } else if (code == KeyEvent.VK_DOWN) {
                System.out.println("DOWN");
                sendToServer(id + " DOWN");
            } else if (code == KeyEvent.VK_RIGHT) {
                System.out.println("RIGHT");
                sendToServer(id + " RIGHT");
            }
        }
    }

    public void resetUserControls() {
        submitUsername.setEnabled(true);
        username.setEnabled(true);
    }

    private void setNoBGbtnPartly(JButton btn) {
        //btn.setBorderPainted(false);
        btn.setContentAreaFilled(false);
        btn.setFocusPainted(false);
        btn.setOpaque(false);
    }

    private void setNoBGbtnFully(JButton btn) {
        btn.setBorderPainted(false);
        btn.setContentAreaFilled(false);
        btn.setFocusPainted(false);
        btn.setOpaque(false);
    }
}
