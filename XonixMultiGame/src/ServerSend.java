import javax.swing.*;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class ServerSend extends Thread {
    private Socket socket;
    private int currID;
    private RunGamePanel rgp;

    public void setRunning(boolean running) {
        isRunning = running;
    }

    private boolean isRunning = true;

    public ServerSend(Socket socket, int currID, RunGamePanel rgp) {
        this.socket = socket;
        this.currID = currID;
        this.rgp = rgp;
    }

    private void sendToClient(String msg) {
        //System.out.println("Server : ");
        try {
            DataOutputStream output = new DataOutputStream(socket.getOutputStream());
            output.writeUTF(msg);
        } catch (IOException e1) {
            System.out.println("Network Problem");
            try {
                Thread.sleep(2000);
                System.exit(0);
            } catch (InterruptedException e2) {

                e2.printStackTrace();
            }
        }
    }

    /*private void sendToClientLabel(JLabel label) {

        try {
            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            output.writeObject(label);
        } catch (IOException e1) {
            e1.printStackTrace();
            System.out.println("Network Problem");
            try {
                Thread.sleep(2000);
                System.exit(0);
            } catch (InterruptedException e2) {

                e2.printStackTrace();
            }
        }
    }*/

    @Override
    public void run() {
        sendToClient(String.valueOf(currID));

        while (isRunning) {
            try {
                sendToClient("INFO " + rgp.getGp().getUserAti(currID).getLabel().getText());
                //sendToClientLabel(rgp.getGp().getUserAti(currID).getLabel());

                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        sendToClient("NEWGAME");

    }
}
